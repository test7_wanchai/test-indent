<table id="balanceTable" class="compact hover row-border">
	<thead>
		<tr>
			<th></th>
			<td><h3 class="small">{{_("Balance sheet")}}</h3></td>
			{{assign var=total_balances value=0}}
			{{section name=companyBalance loop=$company_balance[0].balances}}
				<th class="text-right {{if $total_balances eq 0}} min-phone-p {{elseif $total_balances eq 1}} min-tablet-p {{elseif $total_balances eq 2}} min-tablet-l {{elseif $total_balances eq 3}} desktop {{else}} none {{/if}}">
					<div class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							{{$company_balance[0].balances[companyBalance].date}}
						</a>
						<div class="dropdown-menu">
							<a href="#" onclick="event.preventDefault();" data-toggle="modal" data-target="#compare_balances_modal" data-balance="{{$company_balance[0].balances[companyBalance].balance}}" class="dropdown-item">
								{{_('Compare balancesheet')}}
							</a>
							<a class="dropdown-item" href="./{{$language}}/balance_treemap/{{$company_balance[0].balances[companyBalance].balance}}">
								{{_('Treemap')}}
							</a>
						</div>
					</div>
				</th>
				{{assign var=total_balances value=$total_balances+1}}
			{{/section}}
		</tr>
	</thead>
	<tbody>
		{{section name=companyAccount loop=$company_balance}}
			<tr class="accounts_data" data-absolute-position="{{$company_balance[companyAccount].absolute_position}}" data-position="{{$company_balance[companyAccount].position}}" data-account-type="{{$company_balance[companyAccount].account_type}}" data-id="{{$company_balance[companyAccount].chart_of_accounts}}" data-parent="{{$company_balance[companyAccount].parent_account}}" itemscope itemtype="https://schema.org/DataFeedItem" itemprop="dataFeedElement">
				<td><meta itemprop="description" content="{{sprintf(_('Balance sheet and income statement of company %s presented on Toll & Haben for account %s.'), {{$company->name}}, {{$company_balance[companyAccount].name.text}})}}" /></td>
				<td>
					<small>
						{{if isset($company_balance[companyAccount].change_document)}}
							<a class="text-dark" href="./{{$language}}/discussion/{{$company_balance[companyAccount].change_document->url}}" title="{{$company_balance[companyAccount].change_document->text|escape:'htmlall'}}">
						{{/if}}
						<span class="{{if $company_balance[companyAccount].parent_account != 0}} drag {{/if}}" itemprop="name"{{if isset($company_balance[companyAccount].name.source)}} title="{{$company_balance[companyAccount].name.source}}"{{elseif isset($company_balance[companyAccount].name.english)}} title="{{_('No translation in your language available.')}}"{{/if}}>{{$company_balance[companyAccount].name.text}}</span>
						{{if isset($company_balance[companyAccount].change_document)}}
							</a>
						{{/if}}
					</small>
					{{if !empty($company_balance[companyAccount].instrument)}}
						<div class="d-inline dropdown show">
							<a class="dropdown-toggle text-muted" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" title="Toll & Haben: {{$company_balance[companyAccount].instrument->isin}}" id="instrument{{$company_balance[companyAccount].instrument->isin}}">
								<small>
									{{$company_balance[companyAccount].instrument->isin}}
								</small>
							</a>
							<div class="dropdown-menu" aria-labelledby="instrument{{$company_balance[companyAccount].instrument->isin}}">
								<div class="dropdown-item lead">{{$company_balance[companyAccount].instrument->name}}</div>
								<div class="dropdown-divider"></div>
								<div class="dropdown-item">ISIN: {{$company_balance[companyAccount].instrument->isin}}</div>
								<div class="dropdown-item">ISIX: {{$company_balance[companyAccount].instrument->isix}}</div>
								<div class="dropdown-item">WKN: {{$company_balance[companyAccount].instrument->wkn}}</div>
								{{if ($company_balance[companyAccount].instrument->type == 2)}}
									<div class="dropdown-item">{{_('Instrument Type')}}: {{$company_balance[companyAccount].instrument->instrument_type}}</div>
									<div class="dropdown-item">{{_('Bond Type')}}: {{$company_balance[companyAccount].instrument->bond_type}}</div>
								{{else}}
									<div class="dropdown-item"> {{_('Shares')}}: {{$company_balance[companyAccount].instrument->formatted_shares}}</div>
								{{/if}}
								<div class="dropdown-item">{{_('Price')}}: {{$company_balance[companyAccount].instrument->formatted_price}} {{$company_balance[companyAccount].instrument->currency}}</div>
							</div>
						</div>
					{{/if}}
				</td>
				{{assign var=id_counter value=0}}
				{{section name=companyBalance loop=$company_balance[companyAccount].balances}}
				   {{if !isset($company_balance[companyAccount].balances[companyBalance].account->id)}}
					   {{assign var=account_id value="#$id_counter"}}
					   {{assign var=id_counter value=$id_counter+1}}
				   {{else}}
					   {{assign var=account_id value=$company_balance[companyAccount].balances[companyBalance].account->id}}
				   {{/if}}
					<td class="text-right" data-amount="{{$company_balance[companyAccount].balances[companyBalance].account->formatted_amount}}" data-account-id="{{$account_id}}" data-balance-id="{{$company_balance[companyAccount].balances[companyBalance].balance}}" itemscope itemtype="https://schema.org/MonetaryAmount" itemprop="item">
						<meta itemprop="validFrom" content="{{$company_balance[companyAccount].balances[companyBalance].date}}" />
						<small class="text-nowrap">
							{{if $change_document_exists = !empty($company_balance[companyAccount].balances[companyBalance].account->change_document)}}
								<a class="text-dark" href="./{{$language}}/discussion/{{$company_balance[companyAccount].balances[companyBalance].account->change_document->url}}" title="{{$company_balance[companyAccount].balances[companyBalance].account->change_document->text|escape:'htmlall'}}">
							{{/if}}
							{{if !empty($company_balance[companyAccount].balances[companyBalance].account->amount)}}
								<span itemprop="minValue">{{$company_balance[companyAccount].balances[companyBalance].account->formatted_amount}}</span>
							{{else}}
								0
							{{/if}}
							{{if $change_document_exists}}
								</a>
							{{/if}}
							
							{{if isset($company_balance[companyAccount].balances[companyBalance].currency.change_document)}}
								<a class="text-dark" href="./{{$language}}/discussion/{{$company_balance[companyAccount].balances[companyBalance].currency.change_document->url}}" title="{{$company_balance[companyAccount].balances[companyBalance].currency.change_document->text|escape:'htmlall'}}">
							{{/if}}
							&nbsp;<span itemprop="currency">{{$company_balance[companyAccount].balances[companyBalance].currency.currency}}</span>
							{{if isset($company_balance['companyAccount'].balances[companyBalance].currency.change_document)}}
								</a>
							{{/if}}
						</small>
					</td>
				{{/section}}
			</tr>
		{{/section}}
	</tbody>
</table>
