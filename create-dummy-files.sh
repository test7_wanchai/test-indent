#!/bin/bash

find . -maxdepth 1 -type f -name "dummy-*.txt" -delete

for i in {1..10000}
do
   touch "dummy-$i.txt"
   echo -e "Can you see me? \nHello world! \n\nFile: dummy-$i.txt." > "dummy-$i.txt"
done
